/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dealdey.dealdeysample.dday.pojos;


import java.io.Serializable;

/**
 * @author Ayoola Ajebeku
 */
public class User implements Serializable {

    private String id = "", email = "", name = "", phone = "", password = "";

    public String getId() {
        return id;
    }

    public void setId(String Id) {
        this.id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getVersion() {
        return 1;
    }
}
