/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dealdey.dealdeysample.dday.pojos;


import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author user
 */
public class Deal implements Serializable {

    private String id, short_title, address_line1, address_line2, display_sold_percentage, cod_available,
            permalink, display_sold_quantity, percent_discount, additional_images, highlights,
            fine_prints, description, main_image, store_ribbon, web_banner_image, mobile_banner_image,
            hover_location, image;
    private int discounted_price, bought_count, master_variant_id, saving, time_left, list_price, percent_sold,
            remaining_quantity, variants_have_same_price;
    private boolean variants_enabled, offline_deal, show_timer, is_product_deal,
            display_remaining_quantity, cancelled, available, carry_go, shippable,
            sold_out, display_sold_out, is_new_deal, presale;

    private ArrayList<String> allImages;

    private String[] variant_images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRemaining_quantity() {
        return remaining_quantity;
    }

    public void setRemaining_quantity(int remaining_quantity) {
        this.remaining_quantity = remaining_quantity;
    }

    public ArrayList<String> getAllImages() {
        return allImages;
    }

    public void setAllImages(ArrayList<String> allImages) {
        this.allImages = allImages;
    }

    public String getShort_title() {
        return short_title;
    }

    public void setShort_title(String short_title) {
        this.short_title = short_title;
    }

    public String getAddress_line1() {
        return address_line1;
    }

    public void setAddress_line1(String address_line1) {
        this.address_line1 = address_line1;
    }

    public String getAddress_line2() {
        return address_line2;
    }

    public void setAddress_line2(String address_line2) {
        this.address_line2 = address_line2;
    }

    public String getDisplay_sold_percentage() {
        return display_sold_percentage;
    }

    public void setDisplay_sold_percentage(String display_sold_percentage) {
        this.display_sold_percentage = display_sold_percentage;
    }

    public String getCod_available() {
        return cod_available;
    }

    public void setCod_available(String cod_available) {
        this.cod_available = cod_available;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getDisplay_sold_quantity() {
        return display_sold_quantity;
    }

    public void setDisplay_sold_quantity(String display_sold_quantity) {
        this.display_sold_quantity = display_sold_quantity;
    }

    public String getPercent_discount() {
        return percent_discount;
    }

    public void setPercent_discount(String percent_discount) {
        this.percent_discount = percent_discount;
    }

    public int getSaving() {
        return saving;
    }

    public void setSaving(int saving) {
        this.saving = saving;
    }

    public String getAdditional_images() {
        return additional_images;
    }

    public void setAdditional_images(String additional_images) {
        this.additional_images = additional_images;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getFine_prints() {
        return fine_prints;
    }

    public void setFine_prints(String fine_prints) {
        this.fine_prints = fine_prints;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public int getDiscounted_price() {
        return discounted_price;
    }

    public void setDiscounted_price(int discounted_price) {
        this.discounted_price = discounted_price;
    }

    public int getBought_count() {
        return bought_count;
    }

    public void setBought_count(int bought_count) {
        this.bought_count = bought_count;
    }

    public int getMaster_variant_id() {
        return master_variant_id;
    }

    public void setMaster_variant_id(int master_variant_id) {
        this.master_variant_id = master_variant_id;
    }

    public int getTime_left() {
        return time_left;
    }

    public void setTime_left(int time_left) {
        this.time_left = time_left;
    }

    public int getList_price() {
        return list_price;
    }

    public void setList_price(int list_price) {
        this.list_price = list_price;
    }

    public int getPercent_sold() {
        return percent_sold;
    }

    public void setPercent_sold(int percent_sold) {
        this.percent_sold = percent_sold;
    }

    public boolean isVariants_enabled() {
        return variants_enabled;
    }

    public void setVariants_enabled(boolean variants_enabled) {
        this.variants_enabled = variants_enabled;
    }

    public boolean isOffline_deal() {
        return offline_deal;
    }

    public void setOffline_deal(boolean offline_deal) {
        this.offline_deal = offline_deal;
    }

    public boolean isShow_timer() {
        return show_timer;
    }

    public void setShow_timer(boolean show_timer) {
        this.show_timer = show_timer;
    }

    public boolean is_product_deal() {
        return is_product_deal;
    }

    public void setIs_product_deal(boolean is_product_deal) {
        this.is_product_deal = is_product_deal;
    }

    public boolean isDisplay_remaining_quantity() {
        return display_remaining_quantity;
    }

    public void setDisplay_remaining_quantity(boolean display_remaining_quantity) {
        this.display_remaining_quantity = display_remaining_quantity;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isCarry_go() {
        return carry_go;
    }

    public void setCarry_go(boolean carry_go) {
        this.carry_go = carry_go;
    }

    public boolean isShippable() {
        return shippable;
    }

    public void setShippable(boolean shippable) {
        this.shippable = shippable;
    }

    public int getVariants_have_same_price() {
        return variants_have_same_price;
    }

    public void setVariants_have_same_price(int variants_have_same_price) {
        this.variants_have_same_price = variants_have_same_price;
    }

    public boolean isSold_out() {
        return sold_out;
    }

    public void setSold_out(boolean sold_out) {
        this.sold_out = sold_out;
    }

    public String[] getVariant_images() {
        return variant_images;
    }

    public void setVariant_images(String[] variant_images) {
        this.variant_images = variant_images;
    }

    public String getStore_ribbon() {
        return store_ribbon;
    }

    public void setStore_ribbon(String store_ribbon) {
        this.store_ribbon = store_ribbon;
    }

    public String getWeb_banner_image() {
        return web_banner_image;
    }

    public void setWeb_banner_image(String web_banner_image) {
        this.web_banner_image = web_banner_image;
    }

    public String getMobile_banner_image() {
        return mobile_banner_image;
    }

    public void setMobile_banner_image(String mobile_banner_image) {
        this.mobile_banner_image = mobile_banner_image;
    }

    public String getHover_location() {
        return hover_location;
    }

    public void setHover_location(String hover_location) {
        this.hover_location = hover_location;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isDisplay_sold_out() {
        return display_sold_out;
    }

    public void setDisplay_sold_out(boolean display_sold_out) {
        this.display_sold_out = display_sold_out;
    }

    public boolean is_new_deal() {
        return is_new_deal;
    }

    public void setIs_new_deal(boolean is_new_deal) {
        this.is_new_deal = is_new_deal;
    }

    public boolean isPresale() {
        return presale;
    }

    public void setPresale(boolean presale) {
        this.presale = presale;
    }

}
