package com.dealdey.dealdeysample.dday.pojos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 6/9/15.
 */
public class DealListing implements Serializable {
    private int cart_items_count, per_page, total_pages, current_page, total_entries;
    private boolean success;
    private ArrayList<Deal> deals;

    public int getCart_items_count() {
        return cart_items_count;
    }

    public void setCart_items_count(int cart_items_count) {
        this.cart_items_count = cart_items_count;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public int getTotal_entries() {
        return total_entries;
    }

    public void setTotal_entries(int total_entries) {
        this.total_entries = total_entries;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<Deal> getDeals() {
        return deals;
    }

    public void setDeals(ArrayList<Deal> deals) {
        this.deals = deals;
    }
}