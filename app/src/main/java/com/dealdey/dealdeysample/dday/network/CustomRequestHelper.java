package com.dealdey.dealdeysample.dday.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by user on 3/24/15.
 */
public class CustomRequestHelper {

    private final Context context;

    public CustomRequestHelper(Context context) {
        this.context = context;
    }

    //Make a sycnrhonous internet request to server.
    public String internetConnect(int method, String url, final HashMap params) {
        final RequestQueue requestQueue = Volley.newRequestQueue(context);
        //Using RequestFuture makes the volley to run synchronously
        final RequestFuture<String> requestFuture = RequestFuture.newFuture();
        final CustomRequest request;
        request = new CustomRequest(context, method, url, params, requestFuture, requestFuture);
        requestQueue.add(request);

        try {
            //timeout after 20 seconds.
//            final String response = requestFuture.get(20, TimeUnit.SECONDS);
            return requestFuture.get(20, TimeUnit.SECONDS);
        } catch (Throwable e) {
            if (e instanceof ExecutionException) {
//                e = e.getCause();
                try {
                    System.err.println("Failed.....");
                    System.err.println(new String(((ServerError) e).networkResponse.data, "utf-8"));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                e.printStackTrace();
            }
            return null;
        }
    }
}