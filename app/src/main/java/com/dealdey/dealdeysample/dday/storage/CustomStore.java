package com.dealdey.dealdeysample.dday.storage;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by mac on 3/23/15.
 */
public class CustomStore {

    public static void write(Object obj, String name, Context context) {
        try {
            FileOutputStream fos;
            ObjectOutputStream os;
            fos = context.openFileOutput(name, Context.MODE_PRIVATE);
            os = new ObjectOutputStream(fos);
            os.writeObject(obj);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object read(String name, Context context) {
        try {
            FileInputStream fos;
            ObjectInputStream os;
            fos = context.openFileInput(name);
            os = new ObjectInputStream(fos);
            final Object obj = os.readObject();
            os.close();
            fos.close();
            return obj;
        } catch (Exception e) {
//            e.printStackTrace();
            return null;
        }
    }

    public static boolean delete(String name, Context context) {
        return context.deleteFile(name);
    }
}