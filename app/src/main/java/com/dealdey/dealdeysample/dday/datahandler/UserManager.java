package com.dealdey.dealdeysample.dday.datahandler;

/*
 * Copyright (c) 2015 Delivery Science
 *
 * Permission is hereby granted, free of charge, to any person within Delivery science
 * obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

import android.content.Context;

import com.dealdey.dealdeysample.dday.pojos.User;
import com.dealdey.dealdeysample.dday.storage.CustomStore;


public class UserManager {
    public static UserManager um;
    public static User user = new User();
    private static boolean isUserReal = false;
    private static Context context;

    public static UserManager getInstance(Context con) {
        if (um == null) {
            um = new UserManager();

            final User u = readUser();
            if (u != null) {
                user = u;
            }

            context = con.getApplicationContext();
            return um;
        }
        context = con;
        return um;
    }

    public User getUser() {
        if (user == null || user.getId().isEmpty()) {
            final User u = readUser();
            if (u != null) {
                user = u;
            }
        }
        return user;
    }
//
//    public User getUser() {
//        final User u = readUser();
//        if (u != null) {
//            return u;
//        }
//        return user;
//    }

    public void setUser(User u) {
        user = u;
        storeUser();
    }

    private static User readUser() {
        return (User) CustomStore.read("user", context);
    }

    private void storeUser() {
        CustomStore.write(user, "user", context);
    }

    public boolean logoutUser() {
        user = null;
        return CustomStore.delete("user", context);
    }

    //Converts the user to a pending user in case the user still has some inventories that are yet to be synced to the server.
    public void convertUserToPendingUser() {
        CustomStore.write(user, "pendingUser", context);
        CustomStore.delete("user", context);
        user = null;
    }

    public User getPendingUser() {
        return (User) CustomStore.read("pendingUser", context);
    }

    public boolean logoutPendingUser() {
        return CustomStore.delete("pendingUser", context);
    }
}