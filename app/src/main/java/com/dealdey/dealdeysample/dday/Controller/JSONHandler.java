/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dealdey.dealdeysample.dday.Controller;

import com.dealdey.dealdeysample.dday.pojos.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * @author Ayoola Ajebeku
 */
public class JSONHandler {

    public String processBasicObject(String res) {
//        System.out.println(res);

        try {
            String s = new JSONObject(res).getString("data");
            s = s.replace('"', ' ');
            s = s.replace('[', ' ');
            s = s.replace(']', ' ').trim();
            return s;
        } catch (JSONException ex) {
//            ex.printStackTrace();
            return null;
        }
    }

    public DealListing processDealListing(String res) {
        try {
            DealListing dealListing = new DealListing();
            final JSONObject root = new JSONObject(res);

            dealListing.setTotal_entries(root.optInt("total_entries"));
            dealListing.setCurrent_page(root.optInt("current_page"));
            dealListing.setPer_page(root.optInt("per_page"));
            dealListing.setTotal_pages(root.optInt("total_pages"));
            dealListing.setCart_items_count(root.optInt("cart_items_count"));
            dealListing.setSuccess(root.getBoolean("success"));

            JSONArray jsonArray = root.getJSONArray("deals");
            int l = jsonArray.length();
            ArrayList<Deal> dealArrayList = new ArrayList<>();
            Deal deal;

            for (int i = 0; i < l; i++) {
                deal = new Deal();
                deal.setId(jsonArray.getJSONObject(i).getString("id"));
                deal.setShort_title(stripNull(jsonArray.getJSONObject(i).optString("short_title")));
                deal.setDisplay_sold_out(jsonArray.getJSONObject(i).optBoolean("display_sold_out?"));
                deal.setHover_location(stripNull(jsonArray.getJSONObject(i).optString("hover_location")));
                deal.setIs_new_deal(jsonArray.getJSONObject(i).optBoolean("is_new_deal?"));
                deal.setPresale(jsonArray.getJSONObject(i).optBoolean("presale?"));
                deal.setImage(stripNull(jsonArray.getJSONObject(i).optString("image")));
                deal.setMobile_banner_image(stripNull(jsonArray.getJSONObject(i).optString("mobile_banner_image")));
                deal.setWeb_banner_image(stripNull(jsonArray.getJSONObject(i).optString("web_banner_image")));

                //least_priced_variant
                deal.setDiscounted_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant").optInt("discounted_price"));
                deal.setList_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant").optInt("list_price"));
                deal.setVariants_have_same_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant")
                        .optInt("variants_have_same_price"));

                dealArrayList.add(deal);
            }
            dealListing.setDeals(dealArrayList);

            return dealListing;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public DealListing processMoreDeals(String res, DealListing dealListing) {
        try {
            final JSONObject root = new JSONObject(res);

            dealListing.setTotal_entries(root.optInt("total_entries"));
            dealListing.setCurrent_page(root.optInt("current_page"));
            dealListing.setPer_page(root.optInt("per_page"));
            dealListing.setTotal_pages(root.optInt("total_pages"));
            dealListing.setCart_items_count(root.optInt("cart_items_count"));
            dealListing.setSuccess(root.getBoolean("success"));

            JSONArray jsonArray = root.getJSONArray("deals");
            int l = jsonArray.length();
            ArrayList<Deal> dealArrayList = dealListing.getDeals();
            Deal deal;

            for (int i = 0; i < l; i++) {
                deal = new Deal();
                deal.setId(jsonArray.getJSONObject(i).getString("id"));
                deal.setShort_title(stripNull(jsonArray.getJSONObject(i).optString("short_title")));
                deal.setDisplay_sold_out(jsonArray.getJSONObject(i).optBoolean("display_sold_out?"));
                deal.setHover_location(stripNull(jsonArray.getJSONObject(i).optString("hover_location")));
                deal.setIs_new_deal(jsonArray.getJSONObject(i).optBoolean("is_new_deal?"));
                deal.setPresale(jsonArray.getJSONObject(i).optBoolean("presale?"));
                deal.setImage(stripNull(jsonArray.getJSONObject(i).optString("image")));
                deal.setMobile_banner_image(stripNull(jsonArray.getJSONObject(i).optString("mobile_banner_image")));
                deal.setWeb_banner_image(stripNull(jsonArray.getJSONObject(i).optString("web_banner_image")));

                //least_priced_variant
                deal.setDiscounted_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant").optInt("discounted_price"));
                deal.setList_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant").optInt("list_price"));
                deal.setVariants_have_same_price(jsonArray.getJSONObject(i).optJSONObject("least_priced_variant")
                        .optInt("variants_have_same_price"));

                dealArrayList.add(deal);
            }
            dealListing.setDeals(dealArrayList);

            return dealListing;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Deal processDetails(String res, Deal deal) {
        try {
            final JSONObject root = new JSONObject(res).getJSONObject("deal");

            deal.setPercent_discount(stripNull(root.optString("percent_discount")));
            deal.setSaving(root.optInt("saving"));
            deal.setHighlights(stripNull(root.optString("highlights")));
            deal.setFine_prints(stripNull(root.optString("fine_prints")));
            deal.setDescription(stripNull(root.optString("description")));
            deal.setMain_image(stripNull(root.optString("main_image")));

            ArrayList<String> allImages = new ArrayList<>();
            allImages.add(stripNull(root.optString("main_image")));

            JSONArray jsonArray = root.getJSONArray("variant_images");
            int l = jsonArray.length();

            for (int i = 0; i < l; i++) {
                allImages.add(jsonArray.getString(i));
            }
            deal.setAllImages(allImages);

            return deal;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String stripNull(String s) {
        return s.equals("null") ? "" : s;
    }
}
