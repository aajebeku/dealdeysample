package com.dealdey.dealdeysample.dday.Controller;

import android.content.Context;

import com.android.volley.Request;
import com.dealdey.dealdeysample.dday.network.CustomRequestHelper;
import com.dealdey.dealdeysample.dday.pojos.*;
import com.dealdey.dealdeysample.dday.storage.CustomStore;

import java.util.HashMap;

/**
 * Created by user on 6/6/15.
 */
public class Processor {
    private final String APIURL = "http://api-staging.dealdey.com/api/v1/";
    private Context context;
    private static Processor p;
    private final JSONHandler js;
    private final CustomRequestHelper net;
    public HashMap<String, Deal> dealDetails = new HashMap<>();
    private HashMap<String, String> hashMap;
    public DealListing dealList;
    public Deal selectedDeal;


    public Processor(Context context) {
        this.context = context;
        js = new JSONHandler();
        net = new CustomRequestHelper(context);

        //Load bought media library
        HashMap<String, Deal> lib = (HashMap) CustomStore.read("details", context);
        if (lib != null) {
            dealDetails = lib;
        }
    }

    public static Processor getInstance(Context con) {
        if (p == null) {
            p = new Processor(con);
        }

        return p;
    }

    public DealListing getDealList() {
        hashMap = new HashMap<>();
//        hashMap.put("access_key", "android-testing");

        String res = net.internetConnect(Request.Method.GET, APIURL + "deals?access_key=android-testing", hashMap);
        if (res != null) {//If request got response from server.
            DealListing list;
            list = this.js.processDealListing(res);
            if (list != null) {
                dealList = list;
                CustomStore.write(list, "deallisting", context);
                return list;
            }

            return new DealListing();//return empty vector if something went wrong
        }
        //returns a cached version if it couldn't connect to the server.
        dealList = (DealListing) CustomStore.read("deallisting", context);
        return dealList;
    }

    public DealListing loadMoreDeals(int page) {
        hashMap = new HashMap<>();
//        hashMap.put("access_key", "android-testing");

        String res = net.internetConnect(Request.Method.GET, APIURL + "deals?access_key=android-testing&page=" + page, hashMap);
        if (res != null) {//If request got response from server.
            DealListing list;
            list = this.js.processMoreDeals(res, dealList);
            if (list != null) {
                dealList = list;
                CustomStore.write(list, "deallisting", context);
                return list;
            }

            return new DealListing();//return empty vector if something went wrong
        }
        //returns a cached version if it couldn't connect to the server.
        dealList = (DealListing) CustomStore.read("deallisting", context);
        return dealList;
    }

    public Deal loadDetails(Deal deal) {
        //get a cache copy of deal detail otherwise load from server.
        Deal dd = dealDetails.get(deal.getId());
        if (dd != null) {
            return dd;
        } else {
            hashMap = new HashMap<>();

            String res = net.internetConnect(Request.Method.GET, APIURL + "deals/" + deal.getId() + "?access_key=android-testing", hashMap);
            if (res != null) {
                Deal d = this.js.processDetails(res, deal);
                if (d != null) {
                    deal = d;
                    dealDetails.put(deal.getId(), deal);
                    CustomStore.write(dealDetails, "details", context);

                    return deal;
                }
            }
            return null;
        }
    }
}
