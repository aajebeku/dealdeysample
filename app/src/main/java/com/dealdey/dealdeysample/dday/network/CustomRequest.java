package com.dealdey.dealdeysample.dday.network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by mac on 3/23/15.
 */
public class CustomRequest extends Request<String> {

    private String rawData;
    private Listener<String> listener;
    private Map<String, String> params;
    private Context context;

    //Constructor for sending rawdata in the request body.
    public CustomRequest(Context context, int method, String url, String rawData,
                         Listener<String> reponseListener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.context = context;
        this.listener = reponseListener;
        this.rawData = rawData;
    }    //Constructor for sending rawdata in the request body.

    public CustomRequest(Context context, int method, String url, Map<String, String> params,
                         Listener<String> responseListener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.context = context;
        this.listener = responseListener;
        this.params = params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        final HashMap<String, String> params = new HashMap<String, String>();

        if (rawData != null && !rawData.isEmpty()) {
            params.put("Content-Type", "application/json");
        } else {
            params.put("Content-Type", "application/x-www-form-urlencoded");
        }
        return params;
    }

    protected Map<String, String> getParams()
            throws AuthFailureError {
        return params;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (rawData != null && !rawData.isEmpty())
            return rawData.getBytes();
        else {
            return super.getBody();
        }
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new String(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (Exception je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        return super.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    @Override
    protected void deliverResponse(String response) {
        listener.onResponse(response);
    }

}
