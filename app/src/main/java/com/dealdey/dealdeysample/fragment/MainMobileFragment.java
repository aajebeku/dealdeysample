package com.dealdey.dealdeysample.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.dealdey.dealdeysample.MainActivity;
import com.dealdey.dealdeysample.R;
import com.dealdey.dealdeysample.adapters.DealListAdapter;
import com.dealdey.dealdeysample.dday.Controller.Processor;
import com.dealdey.dealdeysample.dday.pojos.Deal;
import com.dealdey.dealdeysample.dday.pojos.DealListing;
import com.dealdey.dealdeysample.dday.utils.InfiniteScrollListener;

import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainMobileFragment extends ListFragment {

    private ListView listView;
    private Processor processor;
    private SwipeRefreshLayout swipe;
    private ProgressBar progress;
    private ArrayList<Deal> deals;
    private DealListAdapter dealsListAdapter;

    public MainMobileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("DealDey");
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        processor = Processor.getInstance(getActivity().getApplicationContext());
        deals = processor.dealList.getDeals();

        swipe = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_container);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        final View footer = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.footer, null, false);

        listView = getListView();
        dealsListAdapter = new DealListAdapter(getActivity(), deals);

        listView.addFooterView(footer);

        listView.setAdapter(dealsListAdapter);


        //infinitescroll
        listView.setOnScrollListener(new InfiniteScrollListener((listView.getLastVisiblePosition() - listView.getFirstVisiblePosition()) + 1) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                if (processor.dealList.getCurrent_page() != processor.dealList.getTotal_pages()) {
                    new Thread() {
                        public void run() {
                            DealListing dealListing = processor.loadMoreDeals(processor.dealList.getCurrent_page() + 1);
                            deals = dealListing.getDeals();

                            listView.post(new Runnable() {
                                @Override
                                public void run() {
                                    int currentPosition = listView.getFirstVisiblePosition();
//                                    dealsListAdapter.clear();
//                                    dealsListAdapter.addAll(deals);
                                    dealsListAdapter.notifyDataSetChanged();
//                                    listView.setAdapter(new DealListAdapter(getActivity(), deals));
                                    // Setting new scroll position
//                                    listView.setSelectionFromTop(currentPosition + 1, 0);
                                }
                            });
                        }
                    }.start();
                } else {
                    footer.setVisibility(View.GONE);
//                    listView.removeFooterView(footer);
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                processor.selectedDeal = deals.get(i);

                DetailFragment detailFragment = new DetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("selectedDeal", deals.get(i));
                detailFragment.setArguments(b);

                getActivity().getSupportFragmentManager().beginTransaction().
                        replace(R.id.content, detailFragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void refresh() {
        swipe.setRefreshing(true);
        new Thread() {
            public void run() {
                processor.getDealList();

                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        dealsListAdapter.notifyDataSetChanged();
//                        listView.setAdapter(new DealListAdapter(getActivity(), deals));
                        swipe.setRefreshing(false);
                    }
                });
            }
        }.start();
    }
}
