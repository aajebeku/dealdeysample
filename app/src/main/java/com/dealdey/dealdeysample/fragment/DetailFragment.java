package com.dealdey.dealdeysample.fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dealdey.dealdeysample.MainActivity;
import com.dealdey.dealdeysample.R;
import com.dealdey.dealdeysample.adapters.ImageAdapter;
import com.dealdey.dealdeysample.dday.Controller.Processor;
import com.dealdey.dealdeysample.dday.pojos.Deal;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.Calendar;
import java.util.Date;


/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment {

    private Deal selectedDeal;
    private Processor processor;
    private ProgressBar progress;
    private TextView title, listPrice, discount, percent, saving, time;
    private WebView highlight, fineprint, description;
    private ViewPager viewPager;
    private CirclePageIndicator circlePageIndicator;
    private ImageButton share;

    public DetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        Bundle b = getArguments();
        if (b != null) {
            selectedDeal = (Deal) b.getSerializable("selectedDeal");
        }
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(selectedDeal.getShort_title());
        ((MainActivity) getActivity()).getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.err.println("LOL");
                getFragmentManager().popBackStack();
            }
        });

        //

        title = (TextView) getActivity().findViewById(R.id.title);
        share = (ImageButton) getActivity().findViewById(R.id.share);
        time = (TextView) getActivity().findViewById(R.id.time);
        title = (TextView) getActivity().findViewById(R.id.title);

        listPrice = (TextView) getActivity().findViewById(R.id.listPrice);
        listPrice.setPaintFlags(listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        discount = (TextView) getActivity().findViewById(R.id.discount);
        percent = (TextView) getActivity().findViewById(R.id.percent);
        saving = (TextView) getActivity().findViewById(R.id.saving);

        highlight = (WebView) getActivity().findViewById(R.id.highlight);
        fineprint = (WebView) getActivity().findViewById(R.id.fineprint);
        description = (WebView) getActivity().findViewById(R.id.description);


        viewPager = (ViewPager) getActivity().findViewById(R.id.view_pager);
        circlePageIndicator = (CirclePageIndicator) getActivity().findViewById(R.id.indicator);

        progress = (ProgressBar) getActivity().findViewById(R.id.progress);
        processor = Processor.getInstance(getActivity().getApplicationContext());

        loadDetails();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void loadDetails() {
        new Thread() {
            public void run() {
                selectedDeal = processor.loadDetails(selectedDeal);

                viewPager.post(new Runnable() {
                    @Override
                    public void run() {

                        if (selectedDeal != null) {
                            title.setText(selectedDeal.getShort_title());
                            listPrice.setText(Html.fromHtml("<del>N</del>") + "" + selectedDeal.getList_price());
                            discount.setText(Html.fromHtml("<del>N</del>") + "" + selectedDeal.getDiscounted_price());

                            percent.setText(selectedDeal.getPercent_discount() + "%");
                            saving.setText("" + selectedDeal.getSaving());

                            highlight.loadDataWithBaseURL(null, selectedDeal.getHighlights(), "text/html", "utf-8", null);
                            fineprint.loadDataWithBaseURL(null, selectedDeal.getFine_prints(), "text/html", "utf-8", null);
                            description.loadDataWithBaseURL(null, selectedDeal.getDescription(), "text/html", "utf-8", null);

                            viewPager.setAdapter(new ImageAdapter(getActivity(), selectedDeal.getAllImages()));
                            circlePageIndicator.setViewPager(viewPager);
//
//                            progress.setVisibility(View.GONE);

                            System.err.println(selectedDeal.getTime_left() + "................");

                        } else {
                            Toast.makeText(getActivity(), "Internet connection problem, please check your connection.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }.start();
    }

    private void startTimer() {
        new Thread() {
            public void run() {
                time.post(new Runnable() {
                    @Override
                    public void run() {
                        Calendar calendar = Calendar.getInstance();

                        if (selectedDeal.getTime_left() > calendar.getTime().getTime()) {
                            selectedDeal.getTime_left();
                            Date dateLeft = new Date(selectedDeal.getTime_left());
                        }
                    }
                });
            }
        }.start();
    }
}
