package com.dealdey.dealdeysample.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.dealdey.dealdeysample.MainActivity;
import com.dealdey.dealdeysample.R;
import com.dealdey.dealdeysample.adapters.DealListAdapter;
import com.dealdey.dealdeysample.dday.Controller.Processor;
import com.dealdey.dealdeysample.dday.pojos.Deal;
import com.dealdey.dealdeysample.dday.pojos.DealListing;
import com.dealdey.dealdeysample.dday.utils.InfiniteScrollListener;

import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainTabletFragment extends Fragment implements AdapterView.OnItemClickListener {

    private Processor processor;
    private SwipeRefreshLayout swipe;
    private ProgressBar progress;
    private ArrayList<Deal> deals;
    private GridView gridView;
    private View footer;
    private DealListAdapter dealListAdapter;

    public MainTabletFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("DealDey");
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();

        processor = Processor.getInstance(getActivity().getApplicationContext());
        deals = processor.dealList.getDeals();

        swipe = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipe_container);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        footer = getActivity().findViewById(R.id.footer);

        gridView = (GridView) getView().findViewById(R.id.gridView);
        dealListAdapter = new DealListAdapter(getActivity(), deals);
        gridView.setAdapter(dealListAdapter);
        //infinitescroll
        gridView.setOnScrollListener(new InfiniteScrollListener((gridView.getLastVisiblePosition() - gridView.getFirstVisiblePosition()) + 1) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                if (processor.dealList.getCurrent_page() != processor.dealList.getTotal_pages()) {
                    footer.setVisibility(View.VISIBLE);

                    new Thread() {
                        public void run() {
                            DealListing dealListing = processor.loadMoreDeals(processor.dealList.getCurrent_page() + 2);
                            deals = dealListing.getDeals();

                            gridView.post(new Runnable() {
                                @Override
                                public void run() {
                                    footer.setVisibility(View.GONE);

                                    int currentPosition = gridView.getFirstVisiblePosition();
                                    dealListAdapter.notifyDataSetChanged();
//                                    gridView.setAdapter(new DealListAdapter(getActivity(), deals));
                                    // Setting new scroll position
                                    gridView.setSelection(currentPosition + 1);
//                                            .setSelectionFromTop(currentPosition + 1, 0);
                                }
                            });
                        }
                    }.start();
                }
            }
        });
        gridView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        processor.selectedDeal = deals.get(i);

        DetailFragment detailFragment = new DetailFragment();
        Bundle b = new Bundle();
        b.putSerializable("selectedDeal", deals.get(i));
        detailFragment.setArguments(b);

        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.content, detailFragment).addToBackStack(null).commit();
    }

    public void refresh() {
        swipe.setRefreshing(true);
        new Thread() {
            public void run() {
                processor.getDealList();

                gridView.post(new Runnable() {
                    @Override
                    public void run() {
                        dealListAdapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
                    }
                });
            }
        }.start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
