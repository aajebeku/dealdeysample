package com.dealdey.dealdeysample;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dealdey.dealdeysample.dday.utils.CustomUtils;
import com.dealdey.dealdeysample.dday.gtm.Utils;
import com.dealdey.dealdeysample.fragment.MainMobileFragment;
import com.dealdey.dealdeysample.fragment.MainTabletFragment;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setCollapsible(true);
        toolbar.setTitle("DealDey");

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (CustomUtils.isTablet(this)) {
            fragmentTransaction.replace(R.id.content, new MainTabletFragment()).commit();
        } else {
            fragmentTransaction.replace(R.id.content, new MainMobileFragment()).commit();
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.change_state) {
            return true;
        } else if (id == R.id.signin) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.pushOpenScreenEvent(this, "MainScreen");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.pushCloseScreenEvent(this, "MainScreen");
    }

}
