package com.dealdey.dealdeysample.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dealdey.dealdeysample.R;
import com.dealdey.dealdeysample.dday.pojos.Deal;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class DealListAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private ArrayList<Deal> list;
    private Deal deal;

    public DealListAdapter(Activity activity, ArrayList<Deal> list) {
//        super(activity, R.layout.rend_deal_list, list);
        this.activity = activity;
        this.list = list;
//        this.isTablet = isTablet;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.rend_deal_list, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.from = (TextView) convertView.findViewById(R.id.from);
            viewHolder.from.setVisibility(View.GONE);
            viewHolder.discount = (TextView) convertView.findViewById(R.id.discount);

            viewHolder.listPrice = (TextView) convertView.findViewById(R.id.listPrice);
            viewHolder.listPrice.setPaintFlags(viewHolder.listPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            viewHolder.hoverLocation = (TextView) convertView.findViewById(R.id.hoverLocation);
            viewHolder.productImage = (ImageView) convertView.findViewById(R.id.productImage);
            convertView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder)convertView.getTag();
        deal = list.get(position);

        viewHolder.title.setText(deal.getShort_title() + " ");
        viewHolder.discount.setText(Html.fromHtml("<del>N</del>") + "" + deal.getDiscounted_price());
        viewHolder.listPrice.setText(Html.fromHtml("<del>N</del>") + "" + deal.getList_price());
        viewHolder.hoverLocation.setText(deal.getHover_location());

        if (deal.is_new_deal()) {
            viewHolder.isNewImage.setVisibility(View.VISIBLE);
        }

        Picasso.with(activity).load(deal.getImage()).into(viewHolder.productImage);

        return convertView;
    }

    static class ViewHolder {
        TextView title, from, discount, listPrice, hoverLocation;
        ImageView isNewImage, productImage;
    }
}