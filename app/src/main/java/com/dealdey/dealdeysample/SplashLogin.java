package com.dealdey.dealdeysample;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;

import com.dealdey.dealdeysample.dday.Controller.Processor;
import com.dealdey.dealdeysample.dday.gtm.ContainerHolderSingleton;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.tagmanager.Container;
import com.google.android.gms.tagmanager.ContainerHolder;
import com.google.android.gms.tagmanager.TagManager;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by mac on 3/23/15.
 */
public class SplashLogin extends AppCompatActivity {

    private final String CONTAINER_ID = "GTM-5H4FKJ";
    private View splashProgress;
    private ImageView logoImage;
    private Animation anim;
    private View failLayout;
    private Button retry;
    private Processor processor;
//    private int longAnimationDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        processor = Processor.getInstance(getApplicationContext());

        failLayout = (View) findViewById(R.id.failLayout);
        logoImage = (ImageView) findViewById(R.id.logoImage);

        retry = (Button) findViewById(R.id.retry);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoImage.setVisibility(View.VISIBLE);
                failLayout.setVisibility(View.GONE);
                new Thread() {
                    public void run() {
                        enterApp();
                    }
                }.start();
            }
        });

        new Thread() {
            public void run() {
                try {
                    int waited = 0;
                    while (waited < 1500) {
                        sleep(100);
                        waited += 100;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    enterApp();
                }
            }
        }.start();
    }

    private void enterApp() {
        System.err.println("Starting");
        if (processor.getDealList() != null) {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    loadTag();

                }
            });
        } else {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    logoImage.setVisibility(View.GONE);
                    failLayout.setVisibility(View.VISIBLE);

                }
            });
        }
    }

    private void loadTag() {
        TagManager tagManager = TagManager.getInstance(this);
        PendingResult<ContainerHolder> pending =
                tagManager.loadContainerPreferNonDefault(CONTAINER_ID,
                        R.raw.gtm_default_container);
        tagManager.setVerboseLoggingEnabled(true);
        // The onResult method will be called as soon as one of the following happens:
//     1. a saved container is loaded
//     2. if there is no saved container, a network container is loaded
//     3. the request times out. The example below uses a constant to manage the timeout period.
        pending.setResultCallback(new ResultCallback<ContainerHolder>() {
            @Override
            public void onResult(ContainerHolder containerHolder) {
                ContainerHolderSingleton.setContainerHolder(containerHolder);
                Container container = containerHolder.getContainer();
                if (!containerHolder.getStatus().isSuccess()) {
                    System.err.println("Failed.");
//                    return;
                }
                ContainerHolderSingleton.setContainerHolder(containerHolder);
                ContainerLoadedCallback.registerCallbacksForContainer(container);
                containerHolder.setContainerAvailableListener(new  ContainerLoadedCallback());

                startActivity(new Intent(SplashLogin.this, MainActivity.class));
                finish();
            }
        }, 2, TimeUnit.SECONDS);
    }


    private static class ContainerLoadedCallback implements ContainerHolder.ContainerAvailableListener {
        @Override
        public void onContainerAvailable(ContainerHolder containerHolder, String containerVersion) {
            // We load each container when it becomes available.
            Container container = containerHolder.getContainer();
            registerCallbacksForContainer(container);
        }

        public static void registerCallbacksForContainer(Container container) {
            // Register two custom function call macros to the container.
            container.registerFunctionCallMacroCallback("increment", new CustomMacroCallback());
            container.registerFunctionCallMacroCallback("mod", new CustomMacroCallback());
            // Register a custom function call tag to the container.
            container.registerFunctionCallTagCallback("custom_tag", new CustomTagCallback());
        }
    }

    private static class CustomMacroCallback implements Container.FunctionCallMacroCallback {
        private int numCalls;

        @Override
        public Object getValue(String name, Map<String, Object> parameters) {
            if ("increment".equals(name)) {
                return ++numCalls;
            } else if ("mod".equals(name)) {
                return (Long) parameters.get("key1") % Integer.valueOf((String) parameters.get("key2"));
            } else {
                throw new IllegalArgumentException("Custom macro name: " + name + " is not supported.");
            }
        }
    }

    private static class CustomTagCallback implements Container.FunctionCallTagCallback {
        @Override
        public void execute(String tagName, Map<String, Object> parameters) {
            // The code for firing this custom tag.
            Log.i("CuteAnimals", "Custom function call tag :" + tagName + " is fired.");
        }
    }
}
